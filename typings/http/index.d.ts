declare module 'http' {
  interface IncomingHttpHeaders {
    'x-forwarded-for'?: string | undefined;
    'x-real-ip'?: string | undefined;
  }
}
