declare module 'mongoose' {
  interface Query {
    op: string;
    _collection?: {
      collectionName: string;
    };
  }
}
