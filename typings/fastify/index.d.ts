import 'fastify';

import { User, Session } from '../../src/entity/user/user';

declare module 'fastify' {
  interface FastifyInstance {
    limiter: Limiter;
    io: SocketServer;
  }

  interface FastifyRequest {
    ctx: Context;
    requester: User;
    session: Session;
  }

  interface FastifyReply {
    error: Error | null;
  }
}
