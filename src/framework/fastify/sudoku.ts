import * as fastify from 'fastify';

import { Logger } from '../../logger';
import { CreateSudoku, ReadSudoku, UpdateSudokuPosition } from '../../usecase/sudoku';
import { SudokuModel } from '../mongo/sudoku';
import { UserModel } from '../mongo/user';

// TODO
interface CreateSudokuBody {}
// TODO
interface UpdateSudokuBody {}

function CreateSudokuHandler(services: { sudokuModel: SudokuModel }): fastify.RouteHandler<{
  Body: CreateSudokuBody;
}> {
  const createSudoku = CreateSudoku(services);
  return async (req, resp) => {
    const sudoku = await createSudoku(req.ctx, req.requester);
    void resp.status(201);
    return sudoku;
  };
}

function ReadSudokuHandler(services: {
  sudokuModel: SudokuModel;
}): fastify.RouteHandler<{ Params: { sudokuId: string } }> {
  const readSudoku = ReadSudoku(services);

  return async (req, resp) => {
    const sudoku = await readSudoku(req.ctx, req.params.sudokuId);

    void resp.status(200);
    return sudoku.Info();
  };
}

function UpdateSudokuPositionHandler(services: {
  sudokuModel: SudokuModel;
}): fastify.RouteHandler<{ Body: UpdateSudokuBody; Params: { sudokuId: string } }> {
  const updateSudokuPosition = UpdateSudokuPosition(services);

  return async (req, resp) => {
    const sudoku = await updateSudokuPosition(req.ctx, req.params.sudokuId, req.body as {});

    void resp.status(200);
    return sudoku.Info();
  };
}

export async function Plugin(
  instance: fastify.FastifyInstance,
  params: fastify.RegisterOptions & {
    userModel: UserModel;
    sudokuModel: SudokuModel;
    logger: Logger;
  },
): Promise<void> {
  instance
    .route({
      method: 'POST',
      url: '/',
      handler: CreateSudokuHandler(params),
    })
    .route({
      method: 'GET',
      url: '/:sudokuId',
      handler: ReadSudokuHandler(params),
    })
    .route({
      method: 'PATCH',
      url: '/:sudokuId',
      handler: UpdateSudokuPositionHandler(params),
    });
}
