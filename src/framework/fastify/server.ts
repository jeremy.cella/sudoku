import cookie from '@fastify/cookie';
import helmet from '@fastify/helmet';
import * as fastify from 'fastify';

import * as middleware from './middleware';
import * as v1 from './v1';
import { Logger } from '../../logger';
import { Context } from '../../misc';
import { Models, Mongo } from '../mongo/mongo';

export interface CorsConfig {
  enabled: boolean;
  origins: string[];
  methods: string[];
}

export interface SessionConfig {
  duration: number; // Session duration in Hours
  regenRatio: number; // Ratio before Session regen (GET /me)
  secure: boolean;
  httpOnly: boolean;
  sameSite: boolean;
}

interface Services extends Models {
  mongo: Mongo;
  logger: Logger;
}

export interface ServerConfig {
  hostname: string;
  port: number;
  shutdownTimeout: number;
  connectionTimeout: number;
  keepAliveTimeout: number;
  maxParamLength: number;
  bodyLimit: number;
  cors: CorsConfig;
  session: SessionConfig;
  services: Services;
}

export class Server {
  private readonly logger: Logger;
  public readonly config: ServerConfig;
  public readonly server: fastify.FastifyInstance;

  public constructor(config: ServerConfig) {
    this.config = config;
    this.logger = config.services.logger;

    // Fastify config: https://www.fastify.io/docs/latest/Reference/Server/
    this.server = fastify.fastify({
      connectionTimeout: this.config.connectionTimeout,
      keepAliveTimeout: this.config.keepAliveTimeout,
      maxParamLength: this.config.maxParamLength,
      bodyLimit: this.config.bodyLimit, // MiB
      ignoreTrailingSlash: true,
      disableRequestLogging: true,
      caseSensitive: true,
      requestIdHeader: middleware.REQUEST_ID_HEADER,
    });
  }

  public async Start(ctx: Context): Promise<void> {
    this.logger.log({
      tracker: ctx.tracker,
      level: ctx.logLevel ?? 'info',
      message: `Starting server v${this.server.version}`,
    });

    await this.server
      .addHook('onRequest', middleware.WelcomeHandler(this.logger))
      .addHook('onResponse', middleware.GoodbyeHandler(this.logger))
      .setNotFoundHandler(middleware.RouteNotFoundHandler)
      .setErrorHandler(
        middleware.FastifyErrorHandler({
          logger: this.logger,
        }),
      )

      .decorateRequest('requester', null)
      .decorateRequest('session', null)
      .decorateRequest('ctx', null)
      .decorateReply('error', null)
      .register(cookie)
      .register(helmet, {
        hsts: false, // will be always set by reverse proxy
        contentSecurityPolicy: false, // will be always set by reverse proxy
      });

    if (this.config.cors.enabled) {
      this.server
        .addHook('onRequest', middleware.CorsHandler(this.config.cors))
        .options('*', middleware.CorsOptionHandler(this.config.cors));
    }

    await this.server.register(v1.Plugin, {
      prefix: '/api/v1',
      config: this.config,
    });

    await this.server.listen({
      port: this.config.port,
      host: this.config.hostname,
    });

    this.logger.log({
      tracker: ctx.tracker,
      level: ctx.logLevel ?? 'info',
      message: 'API ready!',
    });
  }

  public async Close(ctx: Context): Promise<void> {
    await this.server.close();

    this.logger.log({
      tracker: ctx.tracker,
      level: ctx.logLevel ?? 'info',
      message: 'Closed!',
    });
  }

  public URL(): string {
    return `http://${this.config.hostname}:${this.config.port}`;
  }
}
