import { isPast } from 'date-fns';
import * as fastify from 'fastify';

import * as middleware from './middleware';
import {
  ErrTokenInvalid,
  ErrUserSessionExpired,
  ErrUserSessionNotFound,
  User,
  UserAndSession,
} from '../../entity/user';
import { ErrorName, InternalServerError, IsApplicationError } from '../../error';
import { Context } from '../../misc';
import { UserModel } from '../mongo/user';

export function AuthenticateUserBySessionId(services: {
  userModel: UserModel;
}): (ctx: Context, sessionId: string) => Promise<UserAndSession> {
  const { userModel } = services;

  return async (ctx, sessionId) => {
    if (sessionId === undefined || sessionId === '') {
      throw ErrUserSessionNotFound({
        log: 'Session ID is missing or empty',
      });
    }

    let user: User;

    try {
      user = await userModel.FindUserBy(ctx, { 'sessions.id': sessionId });
    } catch (err) {
      if (IsApplicationError(err) && err.errorName === ErrorName.USER_NOT_FOUND) {
        throw ErrUserSessionNotFound({ message: err.message });
      }

      throw InternalServerError(err);
    }

    const session = user.FindSessionById(sessionId);
    if (session === undefined) {
      throw ErrUserSessionNotFound({
        log: `Cannot find session of ${user.email}`,
      });
    }

    if (isPast(session.expiresAt)) {
      await userModel.RemoveUserSessionById(ctx, user.uuid, session.id);

      throw ErrUserSessionExpired({
        log: `Session ${session.uuid} expired since ${session.expiresAt}`,
      });
    }

    return { user, session };
  };
}

export function Authenticate(services: { userModel: UserModel }): (req: fastify.FastifyRequest) => Promise<void> {
  const authenticateUserBySessionId = AuthenticateUserBySessionId(services);

  return async (req): Promise<void> => {
    if (req.cookies[middleware.SESSION_ID_COOKIE] !== undefined) {
      const userAndSession = await authenticateUserBySessionId(req.ctx, req.cookies[middleware.SESSION_ID_COOKIE]);

      req.requester = userAndSession.user;
      req.session = userAndSession.session;
    } else {
      throw ErrTokenInvalid({
        log: 'SessionId not provided',
        errorName: ErrorName.SESSION_ID_MISSING,
      });
    }
  };
}
