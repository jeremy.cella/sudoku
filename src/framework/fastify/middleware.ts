import * as fastify from 'fastify';

import { CorsConfig } from './server';
import {
  ApplicationError,
  ErrCorsMissing,
  ErrorName,
  HttpError,
  HttpErrorBody,
  InternalServerError,
  IsApplicationError,
  IsHttpError,
  NotFound,
} from '../../error';
import { Logger } from '../../logger';
import { NewContext, NewTrackerSync } from '../../misc';

export const REQUEST_ID_HEADER = 'x-request-id';
export const SESSION_ID_COOKIE = 'session';

export function WelcomeHandler(
  logger: Logger,
): (req: fastify.FastifyRequest, resp: fastify.FastifyReply) => Promise<void> {
  return async (req, resp): Promise<void> => {
    req.ctx = NewContext(NewTrackerSync(), 'debug');

    void resp.header(REQUEST_ID_HEADER, req.ctx.tracker);

    logger.log({
      tracker: req.ctx.tracker,
      level: req.ctx.logLevel ?? 'info',
      message: `${req.raw.method} ${req.raw.url}`,
    });
  };
}

export function GoodbyeHandler(
  logger: Logger,
): (req: fastify.FastifyRequest, resp: fastify.FastifyReply) => Promise<void> {
  return async (req, resp) => {
    const message = `${req.raw.method} ${req.raw.url} => ${resp.raw.statusCode}`;

    logger.log({
      level: req.ctx.logLevel ?? 'info',
      tracker: req.ctx.tracker,
      message,
      data: {
        requester: {
          uuid: req.requester?.uuid,
          email: req.requester?.email,
          ip: req.ip,
          ips: req.ips,
        },
        request: {
          params: JSON.stringify(req.params),
          query: JSON.stringify(req.query),
          body: JSON.stringify(req.body),
        },
        referrer: req.headers.referer,
        status: resp.raw.statusCode,
        method: req.raw.method,
        url: req.raw.url,
        routerPath: req.routeOptions.url,
        params: req.params,
        query: req.query,
        duration: Math.floor(resp.elapsedTime), // ms
      },
    });
  };
}

export async function RouteNotFoundHandler(req: fastify.FastifyRequest): Promise<void> {
  throw NotFound({
    message: `Endpoint ${req.raw.method} ${req.raw.url} not found`,
    errorName: ErrorName.ROUTE_NOT_FOUND,
  });
}

export function ErrorHandler(services: {
  logger: Logger;
}): (
  err: fastify.FastifyError | ApplicationError | unknown,
  req: fastify.FastifyRequest,
  resp: fastify.FastifyReply,
) => HttpErrorBody<unknown> {
  const { logger } = services;

  return (err, req, resp): HttpErrorBody<unknown> => {
    let response = InternalServerError(err);
    const log: string | undefined = undefined;

    if (IsHttpError(err)) {
      response = err;
    } else if (IsApplicationError(err)) {
      if (err.status < 500) {
        response = new HttpError({
          message: err.message,
          status: err.status,
          body: {
            status: err.status,
            message: err.message,
            errorName: err.errorName,
            data: err.data,
          },
        });
      }
    }

    logger.log({
      tracker: req.ctx.tracker,
      level: response.status >= 500 ? 'error' : 'warn',
      message: `Error ${response.message}`.trim(),
      data: {
        log,
        requester: {
          uuid: req.requester?.uuid,
          email: req.requester?.email,
          ip: req.ip,
          ips: req.ips,
        },
        referrer: req.headers.referer,
        status: response.status,
        method: req.raw.method,
        url: req.raw.url,
        routerPath: req.routeOptions.url,
        request: {
          params: JSON.stringify(req.params),
          query: JSON.stringify(req.query),
          body: JSON.stringify(req.body),
        },
        response: {
          body: JSON.stringify(response.body),
        },
        duration: Math.floor(resp.elapsedTime), // ms
        error:
          err === undefined
            ? undefined
            : IsApplicationError(err)
              ? {
                  name: err.name,
                  errorName: err.errorName,
                  message: err.message,
                  stack: err.stack,
                  data: err.data,
                }
              : err instanceof Error
                ? {
                    name: err.name,
                    message: err.message,
                    stack: err.stack,
                  }
                : err,
      },
    });

    return response.body;
  };
}

export function FastifyErrorHandler(services: {
  logger: Logger;
}): (
  err: fastify.FastifyError | ApplicationError | unknown,
  req: fastify.FastifyRequest,
  resp: fastify.FastifyReply,
) => HttpErrorBody<unknown> {
  const errorHandler = ErrorHandler(services);

  return (err, req, resp): HttpErrorBody<unknown> => {
    const error = errorHandler(err, req, resp);

    void resp.status(error.status);
    return error;
  };
}

export function CorsOptionHandler(
  config: CorsConfig,
): (req: fastify.FastifyRequest, resp: fastify.FastifyReply) => Promise<void> {
  return async (_, resp): Promise<void> => {
    void resp.header('Access-Control-Allow-Methods', config.methods.join(',')).status(200);
  };
}

export function CorsHandler(
  config: CorsConfig,
): (req: fastify.FastifyRequest, resp: fastify.FastifyReply) => Promise<void> {
  return async (req, resp) => {
    if (req.raw.method === 'GET') {
      return;
    }

    const origin = req.headers['origin'];
    if (origin === undefined) {
      throw ErrCorsMissing({
        message: 'CORS origin missing',
      });
    }

    if (!config.origins.includes(origin)) {
      throw ErrCorsMissing({
        message: 'Unauthorized CORS',
      });
    } else {
      void resp.headers({
        'Access-Control-Allow-Methods': config.methods.join(','),
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin': origin,
      });
    }
  };
}
