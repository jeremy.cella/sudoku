import * as fns from 'date-fns';
import * as fastify from 'fastify';

import * as auth from './auth';
import * as middleware from './middleware';
import { SessionConfig } from './server';
import { Signin, Logout } from '../../usecase/signin';
import { UserModel } from '../mongo/user';

export interface LoginBody {
  email: string;
  password: string;
}

function SigninHandler(services: {
  userModel: UserModel;
  duration: number; // hours
  httpOnly: boolean;
  sameSite: boolean;
  secure: boolean;
}): fastify.RouteHandler<{ Body: LoginBody }> {
  const { httpOnly, sameSite, secure } = services;
  const signin = Signin(services);

  return async (req, resp) => {
    const userAndSession = await signin(req.ctx, req.body);

    void resp.status(200).setCookie(middleware.SESSION_ID_COOKIE, userAndSession.session.id, {
      httpOnly,
      sameSite,
      secure,
      maxAge: fns.differenceInSeconds(userAndSession.session.expiresAt, new Date()),
    });
    return userAndSession.user.Info();
  };
}

function MeHandler(): fastify.RouteHandler {
  return async (req, resp) => {
    void resp.status(200);
    return req.requester.Info();
  };
}

function LogoutHandler(services: { userModel: UserModel }): fastify.RouteHandler {
  const logout = Logout(services);

  return async (req, resp) => {
    await logout(req.ctx, req.requester, req.session.id);

    void resp.clearCookie(middleware.SESSION_ID_COOKIE, {}).status(204);
  };
}

export async function Plugin(
  instance: fastify.FastifyInstance,
  params: fastify.RegisterOptions & {
    userModel: UserModel;
    session: SessionConfig;
  },
): Promise<void> {
  void instance
    .register(async instance =>
      instance.route({
        method: 'POST',
        url: '/signin',
        handler: SigninHandler({
          ...params,
          ...params.session,
        }),
      }),
    )
    .register(async instance =>
      instance
        .addHook('onRequest', auth.Authenticate(params))
        .route({
          method: 'GET',
          url: '/me',
          handler: MeHandler(),
        })
        .route({
          method: 'DELETE',
          url: '/logout',
          handler: LogoutHandler({ ...params, ...params.session }),
        }),
    );
}
