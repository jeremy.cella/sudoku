import * as fastify from 'fastify';

import * as auth from './auth';
import * as login from './login';
import { ServerConfig } from './server';
import * as sudoku from './sudoku';

export async function Plugin(
  instance: fastify.FastifyInstance,
  params: fastify.RegisterOptions & {
    config: ServerConfig;
  },
): Promise<void> {
  const services = {
    ...params.config.services,
    ...params.config,
  };

  await instance
    .register(login.Plugin, {
      prefix: '/login',
      ...services,
      session: params.config.session,
    })
    .register(async instance => {
      instance.addHook('onRequest', auth.Authenticate(services));
      await instance.register(sudoku.Plugin, {
        prefix: '/sudoku',
        ...services,
      });
    });
}
