import * as mongoose from 'mongoose';

import { Model, ModelBaseCtorParams } from './models';
import { Sudoku } from '../../entity/sodoku';
import { Context } from '../../misc';

export interface SudokuDocument extends Sudoku, mongoose.Document {}

export const SudokuCollection = 'sudoku';

function SudokuSchema(): mongoose.Schema {
  return new mongoose.Schema(
    {
      uuid: {
        type: String,
        required: true,
        index: true,
        unique: true,
      },

      name: {
        type: String,
        required: true,
      },

      grid: {
        type: mongoose.Schema.Types.Mixed,
        required: true,
      },

      isDone: {
        type: Boolean,
        required: true,
      },
    },
    {
      versionKey: false,
      timestamps: {
        createdAt: true,
        updatedAt: true,
      },
      minimize: false,
      toJSON: {
        minimize: false,
        getters: false,
        virtuals: false,
        versionKey: false,
      },
    },
  );
}

export const SudokuProjection = {
  uuid: 1,
  name: 1,
  grid: 1,
  isDone: 1,
  updatedAt: 1,
  createdAt: 1,
};

export class SudokuModel extends Model<SudokuDocument> {
  public constructor(params: ModelBaseCtorParams) {
    super(params, SudokuCollection, SudokuSchema());
  }

  public static Build(doc: SudokuDocument): Sudoku {
    return Sudoku.Build(doc);
  }

  // TODO
  public async FindSudoku(ctx: Context, sudokuId: string): Promise<Sudoku> {
    return {} as Sudoku;
  }

  // TODO
  public async UpdateSudoku(ctx: Context, sudokuId: string, update: Partial<Sudoku>): Promise<Sudoku> {
    return {} as Sudoku;
  }

  public async InsertSudoku(ctx: Context, sudoku: Sudoku): Promise<Sudoku> {
    const model = new this.model({
      ...sudoku,
    });

    const doc = await this.save(ctx, model);
    return SudokuModel.Build(doc);
  }
}
