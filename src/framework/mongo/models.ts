/* eslint-disable @typescript-eslint/no-explicit-any */

import { UpdateOptions } from 'mongodb';
import * as mongoose from 'mongoose';
import { MongooseQueryOptions } from 'mongoose';

import { Logger } from '../../logger';
import { Context } from '../../misc';

export interface ModelBaseCtorParams {
  conn: mongoose.Connection;
  logger: Logger;
}

export class Model<T extends mongoose.Document> {
  public readonly collection: string;
  protected readonly conn: mongoose.Connection;
  protected readonly logger: Logger;
  public model: mongoose.Model<T>;

  public constructor(params: ModelBaseCtorParams, collection: string, schema: mongoose.Schema) {
    this.conn = params.conn;
    this.logger = params.logger;
    this.collection = collection;
    this.model = this.conn.model<T>(collection, schema, collection);
  }

  public findOne(
    ctx: Context,
    conditions: mongoose.FilterQuery<T>,
    projections: mongoose.ProjectionType<T>,
    options: mongoose.QueryOptions,
  ): mongoose.Query<T | null, T> {
    return this.model.findOne(conditions ?? {}, projections, options).lean();
  }

  public findOneAndDelete(
    ctx: Context,
    conditions: mongoose.FilterQuery<T>,
    options: mongoose.QueryOptions,
  ): mongoose.Query<T | null, T> {
    return this.model.findOneAndDelete(conditions, options).lean();
  }

  public findOneAndUpdate(
    ctx: Context,
    conditions: mongoose.FilterQuery<T>,
    update: mongoose.UpdateQuery<T>,
    options: mongoose.QueryOptions,
  ): mongoose.Query<T | null, T> {
    return this.model.findOneAndUpdate(conditions, update, options).lean();
  }

  public find(
    ctx: Context,
    conditions: mongoose.FilterQuery<T>,
    projections?: mongoose.ProjectionType<T>,
    options?: mongoose.QueryOptions,
  ): mongoose.Query<T[] | null, T> {
    return this.model.find(conditions, projections, options).lean();
  }

  public async UpdateMany(
    ctx: Context,
    conditions: mongoose.FilterQuery<T>,
    updates: mongoose.UpdateQuery<T>,
    options?: (UpdateOptions & Omit<MongooseQueryOptions<T>, 'lean'>) | null,
  ): Promise<mongoose.Query<any, any>> {
    return this.model.updateMany(conditions, updates, options);
  }

  public UpdateOne(
    ctx: Context,
    conditions: mongoose.FilterQuery<T>,
    update: mongoose.UpdateQuery<T>,
    options?: (UpdateOptions & Omit<MongooseQueryOptions<T>, 'lean'>) | null,
  ): mongoose.Query<any, any> {
    return this.model.updateOne(conditions, update, options);
  }

  public async deleteMany(ctx: Context, conditions: mongoose.FilterQuery<T>): Promise<mongoose.Query<any, any> & any> {
    return this.model.deleteMany(conditions);
  }

  public deleteOne(ctx: Context, conditions: mongoose.FilterQuery<T>): mongoose.Query<any, any> & any {
    return this.model.deleteOne(conditions);
  }

  public aggregate<T>(ctx: Context, ...aggregations: any[]): mongoose.Aggregate<T[]> {
    return this.model.aggregate<T>(aggregations);
  }
  public async save(ctx: Context, document: T, options?: mongoose.SaveOptions): Promise<T> {
    return document.save(options);
  }

  public async insertMany(ctx: Context, documents: T[]): Promise<T[]> {
    return this.model.insertMany(documents) as Promise<T[]>;
  }

  public async bulkWrite(ctx: Context, writes: any[]): Promise<void> {
    await this.model.bulkWrite(writes);
  }
}
