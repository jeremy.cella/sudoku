import * as mongoose from 'mongoose';

import { Model, ModelBaseCtorParams } from './models';
import { Session } from '../../entity/session';
import { ErrUserNotFound, User } from '../../entity/user';
import { Context } from '../../misc';

export interface UserDocument extends User, mongoose.Document {}

export const UserCollection = 'users';

function UserSchema(): mongoose.Schema {
  const userSessionsSchema = new mongoose.Schema(
    {
      uuid: {
        type: String,
        index: true,
        unique: true,
        sparse: true,
      },

      id: {
        type: String,
        index: true,
        unique: true,
        sparse: true,
      },

      expiresAt: Date,
    },
    {
      versionKey: false,
      _id: false,
      minimize: false,
      timestamps: {
        createdAt: true,
        updatedAt: false,
      },
      toJSON: {
        minimize: false,
        getters: false,
        virtuals: false,
        versionKey: false,
      },
    },
  );

  const passwordSchema = new mongoose.Schema(
    {
      hash: {
        type: String,
      },

      salt: {
        type: String,
      },
    },
    {
      versionKey: false,
      minimize: false,
      _id: false,
      timestamps: {
        createdAt: false,
        updatedAt: false,
      },
      toJSON: {
        minimize: false,
        getters: false,
        virtuals: false,
        versionKey: false,
      },
    },
  );

  return new mongoose.Schema(
    {
      uuid: {
        type: String,
        required: true,
        index: true,
        unique: true,
      },

      email: {
        type: String,
        required: true,
        index: true,
        unique: true,
      },

      password: {
        type: passwordSchema,
        default: null,
      },

      firstName: {
        type: String,
      },

      lastName: {
        type: String,
      },

      sessions: {
        type: [userSessionsSchema],
        default: [],
      },
    },
    {
      versionKey: false,
      timestamps: {
        createdAt: true,
        updatedAt: true,
      },
      toJSON: {
        minimize: false,
        getters: false,
        virtuals: false,
        versionKey: false,
      },
      minimize: false,
    },
  );
}

export class UserModel extends Model<UserDocument> {
  public constructor(params: ModelBaseCtorParams) {
    super(params, UserCollection, UserSchema());
  }

  public static Build(doc: UserDocument): User {
    return User.Build(doc);
  }

  public async FindUserBy(
    ctx: Context,
    conditions: {
      uuid?: string;
      email?: string;
      'sessions.id'?: string;
    },
  ): Promise<User> {
    const pipeline: unknown[] = [{ $match: conditions }];

    const users = await this.aggregate<UserDocument>(ctx, pipeline);
    if (users === null || users.length === 0) {
      throw ErrUserNotFound({ message: `User not found`, data: conditions });
    }

    return UserModel.Build(users[0]);
  }

  public async RemoveUserSessionById(ctx: Context, userId: string, sessionId: string): Promise<void> {
    const user = await this.findOneAndUpdate(ctx, { uuid: userId }, { $pull: { sessions: { id: sessionId } } }, {});

    if (user === null) {
      throw ErrUserNotFound({ message: `User ${userId} not found` });
    }
  }

  public async AddUserSession(ctx: Context, userId: string, session: Session): Promise<void> {
    const user = await this.findOneAndUpdate(ctx, { uuid: userId }, { $push: { sessions: session } }, {});

    if (user === null) {
      throw ErrUserNotFound({ message: `User ${userId} not found` });
    }
  }

  public async RemoveUserSessionsById(ctx: Context, userId: string, sessionIds: string[]): Promise<void> {
    const user = await this.findOneAndUpdate(
      ctx,
      { uuid: userId },
      { $pull: { sessions: { id: { $in: sessionIds } } } },
      {},
    );

    if (user === null) {
      throw ErrUserNotFound({ message: `User ${userId} not found` });
    }
  }

  public async InsertUser(ctx: Context, user: User): Promise<User> {
    const model = new this.model({
      ...user,
    });

    const doc = await this.save(ctx, model);
    return UserModel.Build(doc);
  }
}
