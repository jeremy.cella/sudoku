import mongoose from 'mongoose';

import { SudokuModel } from './sudoku';
import { UserModel } from './user';
import { Logger } from '../../logger';
import { Context } from '../../misc';

export interface MongoConfig {
  uri: string;
  options: mongoose.ConnectOptions;
  reconnect?: {
    tries: number;
    interval: number; // milliseconds
  };
}

export interface Models {
  userModel: UserModel;
  sudokuModel: SudokuModel;
}

export class Mongo {
  private readonly config: MongoConfig;
  private readonly logger: Logger;
  protected conn: mongoose.Connection | null;

  public constructor(config: MongoConfig, logger: Logger) {
    this.config = config;
    this.logger = logger;
    this.conn = null;
  }

  public GetConn(): mongoose.Connection {
    if (this.conn === null) {
      throw new Error('Mongo not connected');
    }

    return this.conn;
  }

  public async Connect(ctx: Context): Promise<void> {
    if (this.config.reconnect === undefined) {
      return this.connect(ctx);
    }

    const reconnect = this.config.reconnect;
    let index = 0;
    let error: Error | null = null;

    do {
      if (index !== 0) {
        await new Promise<void>(resolve => {
          this.logger.log({
            tracker: ctx.tracker,
            level: ctx.logLevel ?? 'info',
            message: `Retry connection in ${reconnect.interval}ms`,
          });

          setTimeout(resolve, reconnect.interval);
        });
      }

      try {
        await this.connect(ctx);
        error = null;
        break;
      } catch (err) {
        this.logger.log({
          tracker: ctx.tracker,
          level: 'error',
          message: `Connect failed: ${err}`,
          data: {
            name: err.name,
            message: err.message,
            stack: err.stack,
          },
        });

        error = err;
      }

      index++;
    } while (reconnect.tries === 0 || (index < reconnect.tries && error !== null));

    if (error !== null) {
      throw error;
    }
  }

  private async connect(ctx: Context): Promise<void> {
    const defaultOption = {
      autoIndex: false,
    };

    this.logger.log({
      tracker: ctx.tracker,
      level: ctx.logLevel ?? 'info',
      message: 'Connecting to Mongo...',
    });

    // @see https://mongoosejs.com/docs/connections.html#multiple_connections
    this.conn = await mongoose
      .createConnection(this.config.uri, {
        ...defaultOption,
        ...this.config.options,
        minPoolSize: 5,
        maxPoolSize: 10,
      })
      .asPromise();

    const buildInfo = await this.conn.db.admin().buildInfo();

    this.logger.log({
      tracker: ctx.tracker,
      level: ctx.logLevel ?? 'info',
      message: `Connected to server v${buildInfo.version} with mongoose v${mongoose.version}`,
    });

    this.conn.on('disconnected', () => {
      this.logger.log({
        tracker: ctx.tracker,
        level: ctx.logLevel ?? 'info',
        message: 'Disconnected!',
      });
    });

    this.conn.on('reconnected', () => {
      this.logger.log({
        tracker: ctx.tracker,
        level: ctx.logLevel ?? 'info',
        message: 'Reconnected!',
      });
    });

    this.conn.on('close', () => {
      this.logger.log({
        tracker: ctx.tracker,
        level: ctx.logLevel ?? 'info',
        message: 'Closed!',
      });
    });

    this.conn.on('error', (err: unknown) => {
      this.logger.log({
        tracker: ctx.tracker,
        level: 'error',
        message: `Error: ${err}`,
      });
    });
  }

  public async GetModels(): Promise<Models> {
    const conn = this.GetConn();

    const params = {
      conn,
      logger: this.logger,
    };

    return {
      userModel: new UserModel(params),
      sudokuModel: new SudokuModel(params),
    };
  }

  public async Close(): Promise<void> {
    await this.GetConn().close();
  }
}
