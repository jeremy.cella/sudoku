import { v4 } from 'uuid';

import { RandomBytesSync } from './crypto';

export type UUID = string;

export type Tracker = string;

export type LogLevel = 'error' | 'warn' | 'info' | 'verbose' | 'debug';

export interface Context {
  tracker: Tracker;
  logLevel?: LogLevel;
}

export function NewContext(tracker: Tracker, logLevel?: LogLevel): Context {
  return {
    tracker,
    logLevel,
  };
}

export interface EntityInfo {
  uuid: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export function NewUUID(): UUID {
  return v4();
}

const REQUEST_ID_LENGTH = 32;

export function NewTrackerSync(): Tracker {
  return RandomBytesSync(REQUEST_ID_LENGTH / 2).toString('hex');
}
