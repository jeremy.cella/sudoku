import { BuildConfig, Config as ApiConfig } from './config';
import { Hash } from './crypto';
import { User } from './entity/user';
import { IsApplicationError } from './error';
import { Server } from './framework/fastify/server';
import { Mongo } from './framework/mongo/mongo';
import { Logger, NewLogger } from './logger';
import { Context, NewContext, NewTrackerSync } from './misc';

enum ExitCode {
  SUCCESSFUL_EXIT = 0,
  CONFIG_LOAD_FAILED = 1,
  MONGO_CONNECT_FAILED = 2,
  SERVER_START_FAILED = 3,
  SERVER_SHUTDOWN_FAILED = 4,
  SERVER_SHUTDOWN_TIMEOUT = 5,
}

const timeouts: NodeJS.Timeout[] = [];

async function main(ctx: Context, logger: Logger): Promise<void> {
  let config: ApiConfig;

  logger.log({
    tracker: ctx.tracker,
    level: ctx.logLevel ?? 'info',
    message: 'Starting...',
  });

  try {
    config = await BuildConfig();
  } catch (err) {
    logger.log({
      tracker: ctx.tracker,
      level: 'error',
      message: 'BuildConfig failed',
      data: {
        error: {
          name: err.name,
          message: err.message,
          stack: err.stack,
        },
      },
    });
    process.exit(ExitCode.CONFIG_LOAD_FAILED);
  }

  logger = NewLogger();

  logger.log({
    tracker: ctx.tracker,
    level: ctx.logLevel ?? 'info',
    message: 'Config loaded',
  });

  // Mongo
  const mongo = new Mongo(config.mongo, logger.child({ context: 'MongoDB' }));

  try {
    await mongo.Connect(ctx);
  } catch (err) {
    logger.log({
      tracker: ctx.tracker,
      level: 'error',
      message: 'Mongo.Connect failed',
      data: {
        error: err instanceof Error ? { name: err.name, message: err.message, stack: err.stack } : err,
      },
    });
    process.exit(ExitCode.MONGO_CONNECT_FAILED);
  }

  const models = await mongo.GetModels();

  // Create dummy user
  try {
    await models.userModel.FindUserBy(ctx, { email: 'user@test.fr' });
  } catch (err) {
    const user = new User({ email: 'user@test.fr', password: await Hash('dummy') });
    await models.userModel.InsertUser(ctx, user);
  }

  // Server
  const server = new Server({
    ...config.server,
    services: {
      ...models,
      mongo,
      logger: logger.child({ context: 'Fastify' }),
    },
  });

  try {
    await server.Start(ctx);
  } catch (err) {
    logger.log({
      tracker: ctx.tracker,
      level: 'error',
      message: 'Server.Start failed',
      data: {
        error: IsApplicationError(err)
          ? err.toJSON()
          : err instanceof Error
            ? { name: err.name, message: err.message, stack: err.stack }
            : err,
      },
    });

    process.exit(ExitCode.SERVER_START_FAILED);
  }

  logger.log({
    tracker: ctx.tracker,
    level: ctx.logLevel ?? 'info',
    message: `API ready! ${server.URL()}`,
  });

  const signals: NodeJS.Signals[] = ['SIGTERM', 'SIGINT'];

  signals.forEach(signal =>
    process.on(signal, () => {
      logger.log({
        tracker: ctx.tracker,
        level: ctx.logLevel ?? 'info',
        message: `API received signal ${signal}...`,
      });

      void shutdown(ctx, {
        server,
        mongo,
        logger,
      });

      const timeout = setTimeout(() => {
        logger.log({
          tracker: ctx.tracker,
          level: 'error',
          message: `API shutdown with signal ${signal} timed out (${config.server.shutdownTimeout}ms)`,
        });

        process.exit(ExitCode.SERVER_SHUTDOWN_TIMEOUT);
      }, config.server.shutdownTimeout);

      timeouts.push(timeout);
    }),
  );
}

async function shutdown(
  ctx: Context,
  services: {
    server: Server;
    mongo: Mongo;
    logger: Logger;
  },
): Promise<void> {
  const { server, mongo } = services;

  try {
    await server.Close(ctx);
    await mongo.Close();
  } catch (err) {
    logger.log({
      tracker: ctx.tracker,
      level: 'error',
      message: 'API shutdown failed',
      data: {
        error: err instanceof Error ? { name: err.name, message: err.message, stack: err.stack } : err,
      },
    });

    process.exit(ExitCode.SERVER_SHUTDOWN_FAILED);
  }

  logger.log({
    tracker: ctx.tracker,
    level: ctx.logLevel ?? 'info',
    message: 'API finished',
  });

  timeouts.forEach(clearTimeout);
  logger.close();
  process.exit(ExitCode.SUCCESSFUL_EXIT);
}

const ctx = NewContext(NewTrackerSync());
const logger = NewLogger();

main(ctx, logger)
  .then()
  .catch(err => {
    logger.log({
      tracker: ctx.tracker,
      level: 'error',
      message: `Failure ${err.message}`,
      data: {
        error: IsApplicationError(err)
          ? err.toJSON()
          : {
              name: err.name,
              message: err.message,
              stack: err.stack,
            },
      },
    });

    process.exit(ExitCode.SERVER_START_FAILED);
  });
