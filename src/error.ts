// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface ApplicationErrorCtorParams<T = any>
  extends Omit<Partial<ApplicationError<T>>, 'errorName' | 'status'> {
  errorName: ErrorName;
  status: number;
}

export interface HttpErrorBody<T> {
  status: number;
  message?: string;
  errorName?: ErrorName;
  data?: T;
}

export interface ApplicationErrorJSON<T> {
  name: string;
  message: string;
  log?: string;
  stack?: string;
  errorName: ErrorName;
  status: number;
  data?: T;
  isApplicationError: boolean;
}

export class ApplicationError<T = unknown> extends Error {
  public errorName: ErrorName;
  public status: number;
  public log?: string;
  public data?: T;
  public readonly isApplicationError: boolean = true;

  public constructor(raw: ApplicationErrorCtorParams) {
    super(raw.message);

    this.stack = raw.stack;
    this.name = 'ApplicationError';
    this.errorName = raw.errorName;
    this.log = raw.log;
    this.status = raw.status;
    this.data = raw.data;
  }

  public ErrorName(errorName: ErrorName): ApplicationError<T> {
    this.errorName = errorName;
    return this;
  }

  public toJSON(): ApplicationErrorJSON<T> {
    return {
      name: this.name,
      message: this.message,
      log: this.log,
      stack: this.stack,
      errorName: this.errorName,
      status: this.status,
      data: this.data,
      isApplicationError: this.isApplicationError,
    };
  }
}

export function IsApplicationError(err: unknown): err is ApplicationError {
  return err instanceof Object && (err as ApplicationError).isApplicationError;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function ApplicationErrorBuilder<T = any>(
  initial: ApplicationErrorCtorParams<T>,
): (raw?: Partial<ApplicationError<T>>) => ApplicationError<T> {
  return raw =>
    new ApplicationError<T>({
      ...initial,
      ...raw,
    });
}

export enum ErrorName {
  // Common
  UNKNOWN = 'UNKNOWN',
  BAD_JSON = 'BAD_JSON',
  ROUTE_NOT_FOUND = 'ROUTE_NOT_FOUND',
  VALIDATION = 'VALIDATION',

  // Session
  SESSION_ID_MISSING = 'SESSION_ID_MISSING',
  SESSION_UNAUTHORIZED = 'SESSION_UNAUTHORIZED',
  SESSION_EXPIRED = 'SESSION_EXPIRED',

  // Token
  TOKEN_MISSING = 'TOKEN_MISSING',
  TOKEN_UNAUTHORIZED = 'TOKEN_UNAUTHORIZED',
  TOKEN_INVALID = 'TOKEN_INVALID',
  TOKEN_ALREADY_EXISTS = 'TOKEN_ALREADY_EXISTS',

  // CORS
  UNAUTHORIZED_CORS_ORIGIN = 'UNAUTHORIZED_CORS_ORIGIN',

  // User
  USER_INVALID_PASSWORD = 'USER_INVALID_PASSWORD',
  USER_INVALID_EMAIL = 'USER_INVALID_EMAIL',
  USER_ALREADY_EXISTS = 'USER_ALREADY_EXISTS',
  USER_NOT_FOUND = 'USER_NOT_FOUND',
  USER_BAD_SIGNIN = 'USER_BAD_SIGNIN',
  USER_BAD_PASSWORD = 'USER_BAD_PASSWORD',
  USER_HAS_NO_PASSWORD = 'USER_HAS_NO_PASSWORD',
  INVALID_ID = 'INVALID_ID',

  // Sudoku
  SUDOKU_NOT_FOUND = 'SUDOKU_NOT_FOUND',

  AN_ERROR_OCCURRED = 'AN_ERROR_OCCURRED',
}

export class HttpError extends Error {
  public readonly status: number;
  public readonly body: HttpErrorBody<unknown>;
  public readonly error?: Error | null;

  public constructor(raw: Partial<HttpError>) {
    super(raw.message ?? raw.body?.message);

    this.status = raw.status ?? 500;
    this.body = {
      status: this.status,
      message:
        raw.message !== undefined || raw.body?.message !== undefined
          ? this.message
          : this.status === 400
            ? 'BadRequest'
            : this.status === 401
              ? 'Unauthorized'
              : this.status === 403
                ? 'Forbidden'
                : this.status === 404
                  ? 'NotFound'
                  : this.status === 409
                    ? 'Conflict'
                    : this.status === 503
                      ? 'ServiceUnavailable'
                      : 'InternalServerError',
      errorName: raw.body?.errorName,
      data: raw.body?.data,
    };
    this.error = raw.error;
  }
}

export function InternalServerError(err?: unknown): HttpError {
  return new HttpError({
    status: 500,
    body: {
      status: 500,
      message: 'An error occurred',
      errorName: ErrorName.AN_ERROR_OCCURRED,
    },
    error: err instanceof Error ? err : new Error('An error occurred'),
  });
}

export const ErrCorsMissing = ApplicationErrorBuilder({
  errorName: ErrorName.UNAUTHORIZED_CORS_ORIGIN,
  message: 'CORS',
  status: 401,
});

interface ErrorParams<T> {
  error?: Error;
  message?: string;
  errorName?: ErrorName;
  data?: T;
}
export function IsHttpError(err: unknown): err is HttpError {
  return err instanceof HttpError;
}

export function NotFound<T>(params: ErrorParams<T> = {}): HttpError {
  return new HttpError({
    status: 404,
    body: {
      status: 404,
      message: params.error?.message ?? params.message ?? 'NotFound',
      errorName: params.errorName,
      data: params.data,
    },
  });
}
