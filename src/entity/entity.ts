import { parseISO } from 'date-fns';

import { EntityInfo, NewUUID } from '../misc';

export class Entity<InfoOutput extends EntityInfo> {
  public uuid: string;
  public createdAt?: Date;
  public updatedAt?: Date;

  public constructor(raw: Partial<InfoOutput>) {
    this.uuid = raw.uuid ?? NewUUID();
    this.updatedAt =
      raw.updatedAt === undefined
        ? undefined
        : typeof raw.updatedAt === 'string'
          ? parseISO(raw.updatedAt)
          : raw.updatedAt;
    this.createdAt =
      raw.createdAt === undefined
        ? undefined
        : typeof raw.createdAt === 'string'
          ? parseISO(raw.createdAt)
          : raw.createdAt;
  }

  public Info(): EntityInfo {
    return {
      uuid: this.uuid,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
