import { Entity } from './entity';
import { ApplicationErrorBuilder, ErrorName } from '../error';
import { EntityInfo } from '../misc';
import { GenerateSudoku } from '../usecase/sudoku';

// TODO ne pas oublier de throw cette erreur quand le sudoku n'existe pas
export const ErrSudokuNotFound = ApplicationErrorBuilder({
  errorName: ErrorName.SUDOKU_NOT_FOUND,
  status: 404,
});

export interface SudokuInfo extends EntityInfo {
  name: string;
  grid: number[][];
  isDone: boolean;
}

export class Sudoku extends Entity<SudokuInfo> {
  public name: string;
  public grid: number[][];
  public isDone: boolean;

  public constructor(raw: Partial<Sudoku>) {
    super(raw);

    this.grid = raw.grid ?? GenerateSudoku();
    this.isDone = raw.isDone ?? false;
    this.name = raw.name ?? 'My sudoku';
  }

  public static Build(raw: Partial<Sudoku>): Sudoku {
    return new Sudoku({
      uuid: raw.uuid,
      name: raw.name,
      createdAt: raw.createdAt,
      updatedAt: raw.updatedAt,
    });
  }

  public Info(): SudokuInfo {
    return {
      uuid: this.uuid,
      name: this.name,
      grid: this.grid,
      isDone: this.isDone,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
