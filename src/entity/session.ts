import * as fns from 'date-fns';

import * as crypto from '../crypto';
import { NewUUID } from '../misc';

export type Session = {
  uuid: string;
  id: string;
  expiresAt: Date;
  createdAt: Date;
};
async function generateSession(
  duration: number, // hours
  length: number,
): Promise<Session> {
  const now = new Date();

  return {
    uuid: NewUUID(),
    id: (await crypto.RandomBytes(length)).toString('hex'),
    expiresAt: fns.addHours(now, duration),
    createdAt: now,
  };
}

export async function GenerateSession(
  duration: number, // hours
): Promise<Session> {
  return generateSession(duration, crypto.SESSION_ID_LENGTH);
}

export function IsSessionExpired(session: Session): boolean {
  const now = new Date();

  return fns.isBefore(session.expiresAt, now);
}
