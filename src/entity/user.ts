import { parseISO } from 'date-fns';

import { Entity } from './entity';
import { Session } from './session';
import { HashResult } from '../crypto';
import { ApplicationErrorBuilder, ErrorName } from '../error';
import { EntityInfo } from '../misc';

export const ErrUserNotFound = ApplicationErrorBuilder({
  errorName: ErrorName.USER_NOT_FOUND,
  message: 'User not found',
  status: 404,
});

export const ErrUserBadPassword = ApplicationErrorBuilder({
  errorName: ErrorName.USER_BAD_SIGNIN,
  message: 'Invalid user email or password',
  status: 401,
});

export const ErrUserHasNoPassword = ApplicationErrorBuilder({
  errorName: ErrorName.USER_HAS_NO_PASSWORD,
  message: "User's password is not defined",
  status: 400,
});

export const ErrUserSessionNotFound = ApplicationErrorBuilder({
  message: 'Unauthorized',
  errorName: ErrorName.SESSION_UNAUTHORIZED,
  status: 401,
});

export const ErrUserTokenNotFound = ApplicationErrorBuilder({
  message: 'Unauthorized',
  errorName: ErrorName.SESSION_UNAUTHORIZED,
  status: 401,
});

export const ErrUserSessionExpired = ApplicationErrorBuilder({
  message: 'Unauthorized',
  errorName: ErrorName.SESSION_EXPIRED,
  status: 401,
});

export const ErrTokenInvalid = ApplicationErrorBuilder({
  message: 'Unauthorized',
  errorName: ErrorName.TOKEN_INVALID,
  status: 401,
});

export interface UserInfo extends EntityInfo {
  email: string;
  phone: string;
  firstName: string;
  lastName: string;
}

export interface UserAndSession {
  user: User;
  session: Session;
}

export class User extends Entity<UserInfo> {
  public email: string;
  public phone: string;
  public password: HashResult | null;
  public firstName: string;
  public lastName: string;
  public sessions: Session[];

  public constructor(raw: Partial<User>) {
    super(raw);

    this.email = raw.email ?? '';
    this.phone = raw.phone ?? '';
    this.password = raw.password ?? null;
    this.firstName = raw.firstName ?? '';
    this.lastName = raw.lastName ?? '';
    this.sessions = raw.sessions ?? [];
  }

  public static Build(raw: Partial<User>): User {
    return new User({
      uuid: raw.uuid,
      createdAt: raw.createdAt,
      updatedAt: raw.updatedAt,
      email: raw.email,
      phone: raw.phone,
      password: raw.password,
      firstName: raw.firstName,
      lastName: raw.lastName,
      sessions:
        raw.sessions?.map(session => {
          return {
            uuid: session.uuid,
            id: session.id,
            createdAt: typeof session.createdAt === 'string' ? parseISO(session.createdAt) : session.createdAt,
            expiresAt: typeof session.expiresAt === 'string' ? parseISO(session.expiresAt) : session.expiresAt,
          };
        }) ?? [],
    });
  }

  public Info(): UserInfo {
    return {
      uuid: this.uuid,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      email: this.email,
      phone: this.phone,
      firstName: this.firstName,
      lastName: this.lastName,
    };
  }

  public FindSessionById(sessionId: string): Session | undefined {
    return this.sessions.find(session => session.id === sessionId);
  }
}
