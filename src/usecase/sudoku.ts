import { Sudoku } from '../entity/sodoku';
import { SudokuModel } from '../framework/mongo/sudoku';
import { Context } from '../misc';

export function CreateSudoku(services: {
  sudokuModel: SudokuModel;
}): (ctx: Context, sudokuId: string) => Promise<Sudoku> {
  const { sudokuModel } = services;

  return async (ctx): Promise<Sudoku> => {
    const sudoku = new Sudoku({});
    return sudokuModel.InsertSudoku(ctx, sudoku);
  };
}

/*
 * TODO
 * Pouvoir Lire le sudoku depuis mongodb
 * */
export function ReadSudoku(services: {
  sudokuModel: SudokuModel;
}): (ctx: Context, sudokuId: string) => Promise<Sudoku> {
  const { sudokuModel } = services;

  return async (ctx): Promise<Sudoku> => {
    return {} as Sudoku;
  };
}

/*
 * TODO
 * Pouvoir Update une position du Sudoku
 * Récupérer le sudoku (utiliser la fonction d'au dessus ReadSudoku() pour cela)
 * Si la position n'est pas valide, renvoyer 400
 * Si la position est valide, update le document mongo et regarder si le sudoku est complet (update isDone=true)
 * */
export function UpdateSudokuPosition(services: {
  sudokuModel: SudokuModel;
}): (ctx: Context, sudokuId: string, update: {}) => Promise<Sudoku> {
  const { sudokuModel } = services;
  const readSudoku = ReadSudoku(services);

  return async (ctx): Promise<Sudoku> => {
    return {} as Sudoku;
  };
}

export function GenerateSudoku(): number[][] {
  return [
    [3, 0, 6, 5, 0, 8, 4, 0, 0],
    [5, 2, 0, 0, 0, 0, 0, 0, 0],
    [0, 8, 7, 0, 0, 0, 0, 3, 1],
    [0, 0, 3, 0, 1, 0, 0, 8, 0],
    [9, 0, 0, 8, 6, 3, 0, 0, 5],
    [0, 5, 0, 0, 9, 0, 6, 0, 0],
    [1, 3, 0, 0, 0, 0, 2, 5, 0],
    [0, 0, 0, 0, 0, 0, 0, 7, 4],
    [0, 0, 5, 2, 0, 6, 3, 0, 0],
  ];
}
