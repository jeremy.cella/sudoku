import * as crypto from '../crypto';
import { GenerateSession, IsSessionExpired } from '../entity/session';
import { ErrUserBadPassword, ErrUserHasNoPassword, User, UserAndSession } from '../entity/user';
import { UserModel } from '../framework/mongo/user';
import { Context } from '../misc';

export function Signin(services: {
  userModel: UserModel;
  duration: number; // Days
}): (
  ctx: Context,
  params: {
    email: string;
    password: string;
  },
) => Promise<UserAndSession> {
  const { userModel, duration } = services;
  const removeExpiredSessions = RemoveExpiredSessions(services);

  return async (ctx, params) => {
    const user = await userModel.FindUserBy(ctx, { email: params.email });

    if (user.password === null) {
      throw ErrUserHasNoPassword();
    }

    const isValid = await crypto.Compare(params.password, user.password.hash, user.password.salt);
    if (!isValid) {
      throw ErrUserBadPassword();
    }

    const session = await GenerateSession(duration);
    await removeExpiredSessions(ctx, user);
    await userModel.AddUserSession(ctx, user.uuid, session);

    return { user, session };
  };
}

export function Logout(services: {
  userModel: UserModel;
}): (ctx: Context, requester: User, sessionId: string) => Promise<void> {
  const { userModel } = services;

  return async (ctx, requester, sessionId) => {
    await userModel.RemoveUserSessionById(ctx, requester.uuid, sessionId);
  };
}

function RemoveExpiredSessions(services: { userModel: UserModel }): (ctx: Context, user: User) => Promise<void> {
  const { userModel } = services;

  return async (ctx, user) => {
    const expiredSessions = user.sessions.reduce(
      (acc: string[], session) => (IsSessionExpired(session) ? acc.concat(session.id) : acc),
      [],
    );

    if (expiredSessions.length !== 0) {
      await userModel.RemoveUserSessionsById(ctx, user.uuid, expiredSessions);
    }
  };
}
