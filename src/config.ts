import * as joi from 'joi';

import { ServerConfig } from './framework/fastify/server';
import { MongoConfig } from './framework/mongo/mongo';
import { LoadConfig } from './loadConfig';

const DefaultConfigFilePath = './config/config.yml';

export interface Config {
  server: ServerConfig;
  mongo: MongoConfig;
}

export async function BuildConfig(): Promise<Config> {
  const filePath = DefaultConfigFilePath;

  const result = await LoadConfig<Config>({
    filePath,
    schema: joi.object({
      server: joi
        .object({
          hostname: joi.string().required(),
          port: joi.number().port().required(),
          shutdownTimeout: joi.number().min(0).default(60000),
          connectionTimeout: joi.number().min(0).default(60000),
          keepAliveTimeout: joi.number().min(0).default(120000),
          maxParamLength: joi.number().min(0).default(100),
          bodyLimit: joi.number().min(0).default(15728640),
          cors: joi
            .object({
              enabled: joi.boolean().default(true),
              origins: joi
                .array()
                .items(joi.alternatives(joi.string().valid('*'), joi.string().uri({ scheme: ['http', 'https'] }))),
              methods: joi.array().items(joi.string().valid('GET', 'POST', 'PATCH', 'PUT', 'DELETE', 'OPTIONS', '*')),
            })
            .required(),

          session: joi.object({
            duration: joi.number().min(1).max(720).required(),
            regenRatio: joi
              .number()
              .min(1 / 2)
              .max(4 / 5)
              .default(2 / 3),
            secure: joi.boolean().default(true),
            httpOnly: joi.boolean().default(true),
            sameSite: joi
              .alternatives()
              .try(joi.boolean(), joi.string().valid('strict', 'lax', 'none'))
              .default('lax'),
          }),
        })
        .required(),

      mongo: joi
        .object({
          uri: joi.string(),
          reconnect: joi.object({
            tries: joi.number().min(0),
            interval: joi.number().min(0),
          }),
        })
        .required(),
    }),
    options: { abortEarly: false },
  });

  if (result.error !== undefined) {
    throw result.error;
  }

  return result.value;
}
