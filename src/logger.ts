import * as winston from 'winston';

import { LogLevel, Tracker } from './misc';

export function NewLogger(): Logger {
  const logger = winston.createLogger({
    format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
    exitOnError: false,
  });
  return logger.add(
    new winston.transports.Console({
      level: 'debug',
      format: winston.format.combine(
        winston.format.colorize({
          level: true,
          message: false,
        }),
      ),
    }),
  );
}

export interface Log {
  tracker?: Tracker;
  level: LogLevel;
  message: string;
  context?: string;
  timestamp?: Date;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any;
}

export interface LoggerChildParams {
  context?: string;
  app?: string;
}

export interface Logger {
  log: (log: Log) => void;
  close: () => void;
  child: (data: LoggerChildParams) => Logger;
  isDebugEnabled: () => boolean;
}
