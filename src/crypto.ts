import * as crypto from 'crypto';

const SALT_LENGTH = 64;
const PASSWORD_HASH_LENGTH = 64;
export const SESSION_ID_LENGTH = 128;

export async function RandomBytes(len: number): Promise<Buffer> {
  return new Promise<Buffer>((resolve, reject) =>
    crypto.randomBytes(len, (err: Error | null, random: Buffer) => (err !== null ? reject(err) : resolve(random))),
  );
}

export function RandomBytesSync(len: number): Buffer {
  return crypto.randomBytes(len);
}

export interface HashResult {
  hash: string;
  salt: string;
}

export async function Hash(password: string, salt?: string): Promise<HashResult> {
  if (salt === undefined) {
    salt = (await RandomBytes(SALT_LENGTH / 2)).toString('hex');
  }

  return new Promise<HashResult>((resolve, reject) =>
    crypto.scrypt(password, salt as string, PASSWORD_HASH_LENGTH, (err: Error | null, derivedKey: Buffer) =>
      err !== null
        ? reject(err)
        : resolve({
            salt: salt as string,
            hash: derivedKey.toString('hex'),
          }),
    ),
  );
}

export async function Compare(password: string, cipher: string, salt: string): Promise<boolean> {
  return (await Hash(password, salt)).hash === cipher;
}
