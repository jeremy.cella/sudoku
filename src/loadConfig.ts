import * as fs from 'fs';

import * as joi from 'joi';
import * as yaml from 'yaml';

export interface LoadConfigParams {
  filePath: string;
  schema: joi.ObjectSchema;
  options?: joi.ValidationOptions;
}

export interface LoadConfigResult<T = unknown> extends Omit<joi.ValidationResult, 'value'> {
  value: T;
}

export async function LoadConfig<T>(params: LoadConfigParams): Promise<LoadConfigResult<T>> {
  let config;

  const raw = (await fs.promises.readFile(params.filePath, 'utf-8')).toString();

  try {
    config = yaml.parse(raw);
  } catch (err) {
    if (err instanceof Error) {
      throw new Error(`LoadConfig file '${params.filePath}' failed yaml.parse: ${err.stack}`);
    }
  }

  return params.schema.validate(config, params.options) as LoadConfigResult<T>;
}
