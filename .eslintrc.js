module.exports = {
    env: {
        node: true,
    },
    root: true,
    overrides: [
        {
            files: ['*.yaml', '*.yml'],
            parser: 'yaml-eslint-parser',
            plugins: ['yml'],
            extends: ['plugin:yml/recommended', 'plugin:prettier/recommended'],
            rules: {
                'yml/no-empty-mapping-value': 'off',
            },
        },
        {
            files: ['package.json', 'tsconfig*.json'],
            parser: 'jsonc-eslint-parser',
            plugins: ['jsonc'],
            extends: ['plugin:jsonc/recommended-with-json', 'plugin:prettier/recommended'],
            rules: {
                'jsonc/sort-keys': [
                    'error',
                    {
                        pathPattern: '^$', // Hits the root properties
                        order: [
                            'name',
                            'version',
                            'description',
                            'keywords',
                            'license',
                            'author',
                            'main',
                            'types',
                            'typings',
                            'scripts',
                            'jest',
                            'jest-junit',
                            'dependencies',
                            'devDependencies',
                            'peerDependencies',
                            'build',
                        ],
                    },
                    {
                        pathPattern: '^(?:dev|peer|optional|bundled)?[Dd]ependencies$',
                        order: { type: 'asc' },
                    },
                    {
                        pathPattern: '.*',
                        order: { type: 'asc' },
                    },
                ],
            },
        },
        {
            files: ['*.js', '*.ts', '*tsx', '*.d.ts'],
            parser: '@typescript-eslint/parser',
            parserOptions: {
                ecmaVersion: 'latest',
                sourceType: 'module',
                project: './tsconfig.json',
            },
            plugins: ['unicorn'],
            rules: {
                // eslint
                eqeqeq: 'error',
                'no-unused-vars': 'off',
                'no-console': 'error',
                'no-unsafe-optional-chaining': 'error',
                'no-throw-literal': 'error',
                curly: 'error',
                'prefer-template': 'error',
                'prefer-const': 'error',
                'prefer-promise-reject-errors': 'error',
                'no-lonely-if': 'error',
                'use-isnan': 'error',
                'no-cond-assign': 'error',
                'no-shadow-restricted-names': 'error',

                // @typescript-eslint
                '@typescript-eslint/no-non-null-assertion': 'warn',
                '@typescript-eslint/no-duplicate-enum-values': 'error',
                '@typescript-eslint/no-unused-vars': 'error',
                '@typescript-eslint/no-explicit-any': 'error',
                '@typescript-eslint/no-floating-promises': 'error',
                '@typescript-eslint/method-signature-style': 'error',
                '@typescript-eslint/explicit-function-return-type': 'error',
                '@typescript-eslint/explicit-member-accessibility': 'error',
                '@typescript-eslint/strict-boolean-expressions': 'error',
                '@typescript-eslint/switch-exhaustiveness-check': 'error',

                // import/order
                'import/order': [
                    'error',
                    {
                        alphabetize: {
                            order: 'asc',
                        },
                        groups: ['builtin', 'external', 'internal', ['parent', 'sibling', 'index'], 'object', 'type'],
                        'newlines-between': 'always',
                        pathGroupsExcludedImportTypes: ['builtin'],
                    },
                ],

                // security
                'security/detect-object-injection': 'off',
                'security/detect-non-literal-regexp': 'off',
                'security/detect-non-literal-fs-filename': 'off',
                'security/detect-possible-timing-attacks': 'off',

                // unicorn
                'unicorn/prefer-date-now': 'error',
                'unicorn/prefer-array-find': 'error',
                'unicorn/no-array-push-push': 'error',
                'unicorn/no-useless-spread': 'error',
                'unicorn/error-message': 'warn',
            },
            extends: [
                'eslint:recommended',
                'plugin:@typescript-eslint/eslint-recommended',
                'plugin:@typescript-eslint/recommended',
                'plugin:security/recommended-legacy',
                'plugin:import/recommended',
                'plugin:import/typescript',
                'plugin:prettier/recommended',
            ],
        },
    ],
    settings: {
        'import/resolver': {
            typescript: {},
        },
    },
};
